---
layout: post
status: publish
published: true
title: 'My new blog'
tags:
    - Personal
---

Welcome to my new blog!

I've writen in a simple framework that generates static pages. The framework is called Jekyll_.

I hope I get the hang of it soon and devise how to make it nicer to the eyes. For now, the minima theme is enough for me and I think
I will hang with it for a little while.

Anyway, let's see what happens.

.. _Jekyll: http://jekyllrb.com/
