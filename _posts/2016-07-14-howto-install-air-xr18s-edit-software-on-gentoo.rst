---
layout: post
status: publish
published: true
title: "Howto: install Air XR18's edit software on Gentoo"
date: '2016-07-14 20:55:26 -0500'
tags:
    - GNU & Linux
    - HowTos
    - Creative Commons
    - Community
    - English
    - Gentoo
---

Well, I've made the best buy a GNU & Linux user can make when it comes to pro-audio. I got an Air XR18. This product is one of the
best of it's kind. The preamps, FX and software are awesome.

Yes, it works fine on GNU & Linux and Android. Here's how to make it work on Gentoo, if you have a x86_64/multilib installation.

.. code:: bash

    # enable abi_x86_32 for the required libraries
    cat << EOF > /etc/portage/package.use/air-xr18
        dev-libs/libpthread-stubs abi_x86_32
        media-libs/alsa-lib abi_x86_32
        x11-libs/libX11 abi_x86_32
        x11-libs/libXau abi_x86_32
        x11-libs/libXdmcp abi_x86_32
        x11-libs/libXext abi_x86_32
        x11-libs/libxcb abi_x86_32
        x11-proto/inputproto abi_x86_32
        x11-proto/kbproto abi_x86_32
        x11-proto/xcb-proto abi_x86_32
        x11-proto/xextproto abi_x86_32
        x11-proto/xf86bigfontproto abi_x86_32
        x11-proto/xproto abi_x86_32
        media-libs/freetype abi_x86_32
        sys-libs/zlib abi_x86_32
        app-arch/bzip2 abi_x86_32
        media-libs/libpng abi_x86_32
    EOF

    # update @world
    emerge -ajuDN @world

    # run the software
    # ...

It works fine and it looks really cool! I really recommend you consider the Air XR18 if you're into those things.

I dunno who is pushing GNU & Linux compatibility down there at the Music Group, but it's the right thing to do! Kudos to them!
