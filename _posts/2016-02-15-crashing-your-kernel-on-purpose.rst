---
layout: post
status: publish
published: true
title: Crashing your kernel on purpose
date: '2016-02-15 16:20:14 -0600'
tags:
    - GNU & Linux
    - FOSS
    - HowTos
    - Creative Commons
    - English
---

Ok, that sounds funny, doesn't it? Well, it turns out, sometimes, you want to test your kernel dump or something of the sorts. Here's how to do it:

.. code:: bash

    echo c > /proc/sysrq-trigger

This will, instanly, crash your kernel.

References:

* Todor Tanev from StorPool_.

.. _StorPool: http://storpool.com/
